FROM python:3.10

WORKDIR /app
COPY main.py /app

CMD ["python", "main.py"]