# Лабораторная работа №2

## Информация
Будет выполнено на выделенной тачке с Public IP чтобы нормально было протестировать. 

Операционка будет Ubuntu.

## Установка k8s и minicube
Выполним следующую команду:
```
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl
```

Далее установим minicube:
```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
```

Также установим Docker:
```
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

И запустим minicube:
``` 
minikube start
```

![Minicube](images/minikube_start.png)

## Поднимаем наше гениальное приложение
Напишем код для начала, я буду использовать python3:

app.py
```
from http.server import BaseHTTPRequestHandler, HTTPServer

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b'Hello, world!')

if __name__ == '__main__':
    server = HTTPServer(('0.0.0.0', 8080), SimpleHTTPRequestHandler)
    print("Server started on localhost:8080...")
    server.serve_forever()
```

Тогда создадим Dockerfile для него:
```
FROM python:3

WORKDIR /app
COPY app.py /app

CMD ["python", "app.py"]
```

Соберем этот образ:
```
docker build -t hello-world-python .
docker tag hello-world-python g1phy/hello-world-python
docker push g1phy/hello-world-python
```

Создадим Deployment YAML (deployment.yaml):

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-world-python
spec:
  replicas: 1
  selector:
    matchLabels:
      app: hello-world-python
  template:
    metadata:
      labels:
        app: hello-world-python
    spec:
      containers:
      - name: hello-world-python
        image: g1phy/hello-world-python
        ports:
        - containerPort: 8080
```

apiVersion: Указывает версию API Kubernetes, которая используется для создания этого объекта. В данном случае apps/v1 является стандартной для Deployment.

kind: Тип создаваемого ресурса, в данном случае Deployment.

metadata: Метаданные о ресурсе, включая имя (name).

spec: Спецификация Deployment, описывающая, как должны быть созданы и управляемы поды.

replicas: Количество копий пода, которое нужно поддерживать.

selector: Указывает, какие поды относятся к этому Deployment.

template: Шаблон пода, включая его метаданные и спецификации контейнера.

containers: Список контейнеров, которые должны быть запущены в поде. Указывается имя контейнера, образ (Docker image), и порты.

Создадим Service YAML (service.yaml):

```
apiVersion: v1
kind: Service
metadata:
  name: hello-world-python-service
spec:
  type: NodePort
  ports:
  - port: 8080
    targetPort: 8080
    nodePort: 30007
  selector:
    app: hello-world-python
```

apiVersion: Версия API для Service, здесь это v1.

kind: Тип ресурса, в данном случае Service.

metadata: Метаданные сервиса, включая его имя.

spec: Спецификация сервиса.

type: Тип сервиса; NodePort означает, что сервис будет доступен снаружи кластера через порт на каждом узле.

ports: Описание портов сервиса. port - это порт, доступный внутри кластера, targetPort - порт, на который направляется трафик (обычно совпадает с портом пода), и nodePort - порт, на который будет привязан сервис на всех узлах кластера.

selector: Определяет, какие поды будут связаны с этим сервисом. В данном случае, сервис будет связан с подами, имеющими метку app: hello-world-python.

## Проверка 
```
kubectl get pods
minikube service hello-world-python-service --url
```

Здесь мы увидели все поды и посмотрели по какому URL'у доступно наше приложение

Проверим что сайт доступен
```
wget 192.168.49.2:30007
cat index.html.1
```

![Test](images/test.png)

Как видим, все поднялось, все работает :)

Про докер и какие могут быть плохие и хорошие практики:

Вот плохой, представлен в файле bad_docker_file:

```
FROM nginx:latest

RUN apt-get update && apt-get install -y \
    git \
    curl

COPY . /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
```

Вот хороший (good_docker_file):
```
FROM nginx:stable

RUN apt-get update && apt-get install -y \
    curl \
    && rm -rf /var/lib/apt/lists/*

COPY index.html /usr/share/nginx/html/index.html

CMD ["nginx", "-g", "daemon off;"]
```

Во-первых использование latest в теге образа приводит к нестабильности, вместо него юзаем stable

Во-вторых устанавливаем ненужные пакеты (типа git'a), увеличивает размер образа и время сборки.

В-третьих, мы не чистим apt-кэш после установки пакетов, оно оставляет ненужные файлы в образе

В-четвертых, копируем все файлы в образ, оно может включать и ненужные файлы, это увеличивает размер образа и потенциально ненужные файлы в образе.

Из плохих практик при испольовании контейнера:
1. Мы запускаем контейнер с правами root, увеличивает риск по безопасности, так как любая уязвимость дает root-права.
2. Не используем томы для постоянных данных - потеря данных при удалении или перезапуске контейнера.