# Лабораторная работа №1

## Создание первой машинки
Мы будем создавать три машины в одной локальной (не совсем, виртуальной) сети. 

Первым делом, создадим машину со своим ssh-ключем, чтобы к ней можно было подключиться.

Так как эту лабу я делаю с винды, то воспользуюсь классной штукой для генерации SSH-ключей и подключению по SSH - Termius

![Termius](images/1termius_ssh.png)

Берем отсюда public key, его сохраняем. 

Далее заходим на YC, настраиваем машину, выбираем нашу виртуальную сеть и вставляем public-key, по которому будем коннектиться.

![Cloud](images/2cloud_settings.png)

Создаем.

## Создание двух других машинок

Я продемонстрирую пример как создам вторую машинку, чтобы коннектиться из первой. Для третьей будет абсолютно аналогично.

Заходим на нашу первую машинку, для этого указываем IP, private-key который мы создали и юзера.

Создадим SSH-ключ с этой тачки, по которому будет коннект ко второй и далее сразу заберем наш public-key оттуда:
```
ssh-keygen -t ed25519 -C "matveybunos"
cat ~/.ssh/id_ed25519.pub
```

Вот как выглядит на машине:

![Machine](images/3machine.png)

Далее, создаем вторую машину по данному public-ключу, но чтобы приблизить условия максимально к локальной сети, будем использовать ту же виртуальную сети, отключив публичный доступ:

![Cloud](images/4cloud_settings.png)

Далее, подключившись по SSH из первой тачки ко второй проделываем все абсолютно то же самое.

![SSH](images/5ssh_access.png)

## Перекачка файлов
Итак, у нас есть три машины

У первой публичный адрес: **51.250.16.135**

У второй внутренний адрес: **10.129.0.16**

У третьей внутренний адрес: **10.129.0.36**

Подключимся ко второй машине:

![SSH](images/6ssh_to_seecond.png)

Со второй машины создадим файл:

![Random file](images/7random_file.png)

Далее, качнем его на третью тачку и переименуем в *kek.out*:

![SCP](images/8scpsosa.png)

Подключимся и проверим точно ли все перекачалось:

![Working](images/9working.png)

Вау, все получилось, как же так...