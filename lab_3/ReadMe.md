# Пайплайн для пуша докер образа

Для начала создадим какой-то простой код на Python:

```python
if __name__ == '__main__':
    print('Hello, World!')
```

Получим токен на Gitlab для нашего Runner:
![Token](img/2.png)

Далее, установим docker и gitlab runner на нашу машину:

```bash
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner

sudo gitlab-runner register
```

![Image](img/1.png)

Далее, создадим Dockerfile для нашего приложения:

```dockerfile
FROM python:3.10

WORKDIR /app
COPY main.py /app

CMD ["python", "main.py"]
```

Создадим Container Registry на Yandex Cloud:
![Registry](img/3.png)

Создадим сервисник для работы с Container Registry:
![Service](img/4.png)

Далее, создадим IAM-token для него:
![Token](img/5.png)

Проверим логин:
![Login](img/6.png)

Создадим .gitlab-ci.yml:

```yaml
stages:
  - build
  - push

variables:
  REGISTRY: cr.yandex/crpon8ijg4q5pjmt7urd/test
  IMAGE_TAG: $REGISTRY:$CI_COMMIT_REF_NAME
  DOCKER_TLS_CERTDIR: ""


before_script:
  - docker login --username iam --password $YANDEX_OAUTH_TOKEN cr.yandex

build:
  stage: build
  image: docker:latest
  services:
    - docker:dind

  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
  script:
    - docker build -t $IMAGE_TAG .
    - docker save $IMAGE_TAG > image.tar
  artifacts:
    paths:
      - image.tar
  only:
    - main

push:
  stage: push
  image: docker:latest
  services:
    - docker:dind

  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2

  script:
    - docker load < image.tar
    - docker push $IMAGE_TAG
  dependencies:
    - build
  only:
    - main
```

Добавим IAM в переменные проекта:
![IAM](img/7.png)

Запушим gitlab-ci.yml в репозиторий, а потом и docker + python3

Как видим, пайплайн начал выполняться:
![Pipeline](img/8.png)

Пайплайн успешно выполнился:
![Pipeline](img/9.png)

После выполнения пайплайна, мы можем увидеть наш образ в Container Registry:
![Registry](img/10.png)