# Контейнерная архитектура

**Расскажите про контейнерную архитектуру. В чем отличие от обычных компьютеров? В чем преимущества и недостатки над виртуальными машинами? Есть ли задачи, в которых предпочтительнее НЕ использовать контейнеризацию?**

Контейнерная архитектура — это методика виртуализации, при которой приложения изолируются в "контейнерах". Это отличает их от обычных компьютеров в изолированности (каждое приложение/сервис работает в своем контейнере, оно обеспечивает больше безопасности и понижает зависимость от особенностей хоста) и в эффективности потребления ресурсов (так как они делят ядро ОС хоста, они потребляют меньше ресурсов чем полноценный компьютер)

Если сравнивать контейнеры с ВМ то можно выделить props и cons:

Из преимуществ можно выделить легковесность (они не нуждаются в полной ОС для каждого экземпляра, а делят ядро ОС хоста), довольно быстрые развертывание и остановка, а также портативность – довольно просто перенести между облаками, хостами и т.п.

Из недостатков можно выделить ограниченную изоляцию, так как они менее изолированы, чем полноценные виртуалки. А также на контейнерах мы очень зависим от ядра хостовой ОС, все контейнеры должны юзать одно и то же ядро, ограничивает их гибкость.

Из предпочительности – там где очень высокие требования к безопасности, возможно лучше будет использовать ВМ из-за строгой изоляции. Тяжелые приложения, которые требуют много ресурсов, скорее всего будут лучше работать на ВМ или тачках. Ну а также если нужно запускать приложение на разных ОС, чтобы не собирать под каждую архитектуру, возможно предпочтительнее использовать ВМ.

# Risk Management

**Есть понятие Risk Management. Оно включает в себя несколько стадий управления рисками: мониторинг, выявление, создание планов по избеганию рисков или их принятию. Какие есть риски в облаке? Предложите план управления рисками хотя бы по 2м рискам**

Из такого хорошего риска можно выделить нарушение безопасности данных (особенно, по опыту, связанному с сервисниками)) ), поэтому и существует СИБ, чтобы это предотвращать всё. Также облачные сервисы могут быть подвержены простоям, каким-то сбоям или техническим работам (как например, в Яндексе недавно было авария в одном из дата-центров, что не дало возможности деплоя на некоторое продолжительное время).

Теперь давай рассмотрим Риск Менеджмент для этих двух проблем:
1. Нарушение безопасности данных
      - *Мониторинг*: использование инструментов мониторинга безопасности для отслеживания необычных/подозрительных действий. У СИБа например есть специальные дашборды, по которым они смотрят что произошло. Срабатывают алерты даже при малейших подозрениях.
      - *Выявление*: регулярные аудиты безопасности и оценка уязвимостей чтобы выявить потенциальные проблемы.
      - *Избегание*: развертывание сильных криптографических решений для шифрования данных как в покое, так и в передаче, найм СИБа в команду)
      - *Принятие*: подготовка бюджета и ресурсов на случай данных инцидентов, включая страхование от киберрисков.
2. Проблемы доступности и надежности
      - *Мониторинг*: непрерывный мониторинг производительности и доступности сервисов, вывод это все на большой телевизор в вашей команде (обязательно!!!), регулярное тестирование системы на устойчивость к сбоям.
      - *Выявление*: оценивать SLA поставщиков облачных услуг, анализировать истории простоев и как идет восстановление после инков, проводить стресс-тесты.
      - *Избегание*: разработка и внедрение стратегий резервного копирования и аварийного восстановления.
      - *Принятие*: разработка плана действий на случай простоев, включая временное переключение на второстепенные системы или процедуры.

